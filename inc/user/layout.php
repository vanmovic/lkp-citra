<?php
/**
 * Check whenever user is already logged-in or not.
 * @return none
 */


?>


<!-- Eror Login -->
<?php
function loaderorlogin(){
        //kode php ini kita gunakan untuk menampilkan pesan eror
  if (!empty($_GET['error'])) {
    if ($_GET['error'] == 1) {
      echo '<h3><center><font color="red">Username dan Password kosong!</font></center></h3>';
    }
    else if ($_GET['error'] == 2) {
      echo '<h3><center><font color="red">Username belum diisi!</font></center></h3>';
    }
    else if ($_GET['error'] == 3) {
      echo '<h3><center><font color="red">Password belum diisi!</font></center></h3>';
    }
    else if ($_GET['error'] == 4) {
      echo '<h3><center><font color="red">Admin Username atau Password salah!</font></center></h3>';
    }
    else if ($_GET['error'] == 5) {
      echo '<h3><center><font color="red">Member Username atau Password salah!</font></center></h3>';
    }
  }
}
?>

<!-- Start Load Assets Head -->
<?php
function loadAssetsHead($title = 'index'){
  ?>
  <!DOCTYPE html>
  <?php global $ui_register_bg; echo ($ui_register_bg === 'secondary' ) ? '<html lang="en-us" dir="ltr" class="tm-bg-secondary">' : '<html lang="en-us" dir="ltr" class="tm-bg-primary">' ?>

  <head>
    <meta charset="utf-8">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <title><?php echo $title ?></title>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="./assets/user/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="./assets/user/css/style.css" rel='stylesheet' type='text/css' />
    <link href="./assets/user/css/responsive.css" rel='stylesheet' type='text/css' />    
    <link href='./assets/user/css/font.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="./assets/user/css/etalage.css">
    <script type="text/javascript" src="./assets/user/js/jquery-1.11.1.min.js"></script>
    <script src="./assets/user/js/responsiveslides.min.js"></script>
    <script src="./assets/user/js/jquery.etalage.min.js"></script>
    <script>
      jQuery(document).ready(function($){

        $('#etalage').etalage({
          thumb_image_width: 300,
          thumb_image_height: 400,
          source_image_width: 900,
          source_image_height: 1200,
          show_hint: true,
          click_callback: function(image_anchor, instance_id){
            alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
          }
        });

      });
    </script>
    <script>
      $(function () {
        $("#slider").responsiveSlides({
          auto: true,
          nav: true,
          speed: 500,
          namespace: "callbacks",
          pager: true,
        });
      });
    </script>
    <script type="text/javascript" src="./assets/user/js/hover_pack.js"></script>
    <script src="./assets/user/js/easyResponsiveTabs.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
                  type: 'default', //Types: default, vertical, accordion           
                  width: 'auto', //auto or any width like 600px
                  fit: true   // 100% fit in a container
                });
      });
    </script> 
    <link rel="icon" href="assets/user/images/pharm.png" type="image/png" sizes="16x16">
  </head>
  <?php 
}



function loadassetsheadupload(){
  ?>
  <head>
    <meta charset="UTF-8"/>
    

    <link href="./assets/user/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <script src="./assets/user/js/jquery.min.js"></script>
    <script src="./assets/user/js/fileinput.min.js" type="text/javascript"></script>
    <script src="./assets/user/js/bootstrap.min.js" type="text/javascript"></script>
    
  </head>
  <?php
}

?>
<!-- End Load Assets Head -->



<!-- Start Load Menu -->
<?php
/**

/**
 * Load main menu
 * @return none
 */

function validator(){
  ?>

  <link href="./assets/validator/css/bootstrapValidator.min.css" rel="stylesheet"/>
  <link href="./assets/admin/paneladmin/fonts/css/font-awesome.min.css" rel="stylesheet">
  <script src="./assets/validator/js/bootstrapValidator.min.js" type="text/javascript"></script>
  <?php 
} 

function LoadMenu(){
  ?>
<body>  

  <!-- start header --> 
  <div id="header_social">
    <div class="container">
      <div class="row">
        <!-- company information -->
        <div class="col-lg-7 col-md-8 col-sm-9 col-xs-12">
          <div class="company_cell">
            <ul> 
              <li><i class="fa fa-envelope-o"></i> <a href="" >rynt_riyanto@yahoo.com</a></li>
              <li><i class="fa fa-phone"></i><a href="">(0272) 881018</a></li>
              <li><i class="fa fa-clock-o"></i><a href="" >Mon - Fri : 9:00 -1700</a></li> 
            </ul>
          </div>
        </div>
        
        <!-- social menu -->
        <div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
          <div class="cotact_social top-social pull-right">
            <div class="footer_social pull-right">
              <a href="login">Login</a>
              <a href="http://www.facebook.com/lkpcitra" class="i fa fa-facebook"></a>
              <a href="http://www.twitter.com/lkpcitra" class="i fa fa-twitter"></a>
              <a href="http://www.google.com/lkpcitra" class="i fa fa-google-plus"></a>
              <a href="http://www.pinterest.com/lkpcitra" class="i fa fa-pinterest"></a>
              <a href="http://www.rss.com/lkpcitra" class="i fa fa-rss"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- header stricky menu -->
  <header class="header stricky"> 
    <div class="service_page">
      <div class="container">
        <section id="manu_area" class="manu_service">
        <h2 class="hide">menu</h2>
        <div class="navbar" role="navigation"> 
          <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- logo here -->
          </br><a href="home"><img width="250px" height="70px" src="assets/images/banner.png" alt="Sistem Informasi Akademik Citra Pratama" title="Sistem Informasi Akademik Citra Pratama"></a>
          </div>
          
          <!-- Main menu aea -->
          <div class="navbar-collapse collapse">  
          <!-- Right nav -->
          <ul class="nav navbar-nav main-menu scroll-menu">  
            <li class=""><a href="">HOME<span class="border"></span><span class="caret caret-home"></span></a> 
            </li>
            <li><a href="user/pages/tentang">TENTANG<span class="border"></span></a>
              <ul class="dropdown-menu hidesubmenu">  </ul>
            </li>
             <li><a href="user/pages/mentor">MENTOR<span class="border"></span><span class="caret caret-home"></span></a>
            </li>
            <li><a href="user/pages/contact">HUBUNGI KAMI<span class="border"></span></a>
            </li>

          </ul> 
          </div> 
        </div>  
      </section>  
      </div>
    </div>  
    </header> 
<!--<nav class="navbar navbar-default navbar-fixed-top">

</nav>-->
<?php
}
?>
<!-- End Load Menu -->

<!-- Start Load SliderBar -->
<?php 
function LoadSliderBar(){
  ?>
    <section class="main-slider">
      
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                  
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb=" ./assets/user/img/Fullslider/slider.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                    <img src=" ./assets/user/img/Fullslider/slider1.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
              
                    </li>
                    
                  
                </ul>
                
              <div class="tp-bannertimer"></div>
            </div>
        </div>
    </section>

      <!--- start area -->
  <section id="aboutus_area">
    <div class="container">
      <div class="row">
      
        <!-- section title -->
        <div class="head_title about text-center">
          <h4>Tentang Kami</h4>
          <h2>LKP Citra Pratama | Lembaga Kursus Pendidikan Privat di Sukoharjo</h2>
          <span class="text-center"></span>
          <p>LKP Citra Pratama adalah lembaga bimbingan belajar dan pelatihan di Kecamatan Weru Sukoharjo. Berdiri sejak 31 Mei 2002, kami bergerak di bidang pendidikan. LKP Citra Pratama menghadirkan kursus dan pelatihan di antaranya: Bimbingan belajar SD/SMP/SMU, kursus operator komputer, dan kursus setir mobil.</p>
        </div>
        
        <!-- about icon content -->
        <div class="about_content">
        
          <!-- about one -->
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            
            <div class="single_about">
              <div class="single_icon">
                <span class="flaticon-engineer"></span> 
              </div>
              <div class="single_text">
                <h2>Professional</h2>
                <p>LKP Citra Pratama  </p>
              </div>
            </div>
          </div>
          
          <!-- about two -->
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"> 
            <div class="single_about">
              <div class="single_icon">
                <span class="flaticon-protection3"></span>
              </div>
              <div class="single_text">
                <h2>Terpercaya</h2>
                <p>LKP Citra Pratama </p>
              </div>
            </div>
          </div>
          
          <!-- about three -->
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"> 
            <div class="single_about">
              <div class="single_icon">
                <span class="flaticon-maths5"></span>
              </div>
              <div class="single_text">
                <h2>Ahli di bidangnya</h2>
                <p>LKP Citra Pratama </p>
              </div>
            </div>
          </div> 
        </div>
        <!--- about main content -->
        


  </section>
  <!-- end area -->
<?php 
} 
?>
<!-- End Load SliderBar -->




<!-- Start Load SideBar -->
<?php
/**
 * Generate main menu navigation element
 * @param  string $page Page template to match 
 * @param  string $link Menu link
 * @param  string $name Menu name to display
 * @return none 
 */
function generateNavElement( $page, $link, $name){
  global $ui_register_page; 
  echo ( $ui_register_page == $page ) ? '<li class="uk-active"><a href="'.$link.'">'.$name.'</a></li>' . "\n" : '<li><a href="'.$link.'">'.$name.'</a></li>' . "\n";

}
/**
 * Load main menu
 * @return none
 */


function LoadSideBar()
{?>

      <?php 
    }
    ?>
    <!-- End Load SideBar -->




    <!-- Start LoadJs -->
    <?php function LoadJs()
    {
      ?>
      <script type="text/javascript">
        $(function() {
          var menu_ul = $('.menu > li > ul'),
          menu_a  = $('.menu > li > a');
          menu_ul.hide();
          menu_a.click(function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
              menu_a.removeClass('active');
              menu_ul.filter(':visible').slideUp('normal');
              $(this).addClass('active').next().stop(true,true).slideDown('normal');
            } else {
              $(this).removeClass('active');
              $(this).next().stop(true,true).slideUp('normal');
            }
          });
          
        });
      </script>
      <?php
    }
    ?>
    <!-- End LoadJs -->




    <!-- Start Load Testimoni -->
    <?php
    function LoadTestimony()
    {
      ?>
    </div>
    


  


<?php
}

?>
<!-- End Load Testimoni -->




<!-- Start LoadSearch -->
<?php 
function LoadSearch(){
  ?>
 
  <?php
}
?>
<!-- End LoadSearch -->




<!-- Load Footer Start -->
<?php 
function loadfoot(){
  ?>
    <!---footer start area -->
  <section id="footer_area">
    <div class="container">
      <div class="row">
        <div class="footer_content">
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer_spacing">
            <div class="single_footer">
 <a href="login"><img src="assets/images/bawah.png" style="padding-top:20px;" alt="logo_white_big" class="alignnone size-full wp-image-12908"></a>
            </div>
                      
            
          </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer_spacing">
            <div class="column-three">
            <h2>LKP Citra Pratama</h2>
<p>LKP Citra Pratama adalah lembaga bimbingan belajar dan pelatihan di Kecamatan Weru Sukoharjo. Berdiri sejak 31 Mei 2002, kami bergerak di bidang pendidikan.</p> 
<p>LKP Citra Pratama menghadirkan kursus dan pelatihan di antaranya: Bimbingan belajar SD/SMP/SMU, kursus operator komputer, dan kursus setir mobil.</p>
            </div>

                     <div class="footer_social">
              <a href="http://www.facebook.com" class="i fa fa-facebook"></a>
              <a href="http://www.twitter.com" class="i fa fa-twitter"></a>
              <a href="http://www.google.com" class="i fa fa-google-plus"></a>
              <a href="http://www.pinterest.com" class="i fa fa-pinterest"></a>
              <a href="http://www.rss.com" class="i fa fa-rss"></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer_spacing">
            <div class="column-three">
              <h2>Alamat</h2> 
              <a href="" class="i fa fa-map-marker"></a> JL. Kapten Patimura, No. 16, Ngreco, Weru, 57562, Tawang, Sukoharjo, Kabupaten Sukoharjo, Jawa Tengah 57561<br />
              <a href="" class="i fa fa-envelope-o"></a> rynt_riyanto@yahoo.com <br />
            </div>

          </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer_spacing">
            <div class="column-three">

              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3953.226231685196!2d110.748676!3d-7.765817000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa3f3ff964d5250e4!2sLKP+Citra+Pratama!5e0!3m2!1sen!2sid!4v1497277741590" width="250" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

          </div>

      </div>
    </div>
    <hr class="horizenal" /> 
    <div class="container">
      <div class="row">
        <div class="copy_right_area">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="copy_left">
              <p>&copy;Copyright 2017 <span>LKP Citra Pratama</span></p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
            <div class="copy_right">
              <ul>
                <li><a href="">Home</a></li>
                <li><a href="">FAQ</a></li>
                <li><a href="">About</a></li>
                <li><a href="">Contact</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- footer end area -->

  <!-- footer end area -->
  <!-- jquery library -->
  <script src=" ./assets/user/js/jquery.min.js"></script>  
  <!-- bootstrap -->
  <script src=" ./assets/user/js/bootstrap.min.js"></script> 
  <!-- MixIt UP JS -->
  <script src=" ./assets/user/js/jquery.mixitup.min.js"></script> 
  <script src=" ./assets/user/js/masonry.pkgd.min.js"></script> 
  <!-- jQuery sticky -->
    <script src=" ./assets/user/js/jquery.sticky.js"></script> 
  <!-- Count To JS -->
  <script src=" ./assets/user/js/jquery.countTo.js"></script> 
  <!-- iSotope JS -->
  <script src=" ./assets/user/js/isotope.pkgd.min.js"></script> 
   <!-- Revolution Slider Tools --> 
  <script src=" ./assets/user/js/revolution.min.js"></script> 
   <!-- Gmap Helper -->
  <script src="http://maps.google.com/maps/api/js"></script>
  <script src=" ./assets/user/js/gmap.js"></script> 
  <!-- owlcarousel -->
  <script src=" ./assets/user/js/jquery.bxslider.js"></script>  
  <script src=" ./assets/user/js/owl.carousel.js"></script>  
  <!-- FancyBox -->
  <script src=" ./assets/user/js/jquery.fancybox.pack.js"></script> 
  <!-- SmartMenus jQuery plugin -->
    <script src=" ./assets/user/js/jquery.smartmenus.js"></script> 
    <script src=" ./assets/user/js/jquery.smartmenus.bootstrap.js"></script> 
  <!-- my js -->
  <script src=" ./assets/user/js/main.js"></script>   

</body>
</html>   
<?php
}

function DataHeadTabel(){
  ?>
  

  <link href="./assets/admin/paneladmin/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="./assets/admin/paneladmin/css/animate.min.css" rel="stylesheet">
  <link href="./assets/admin/paneladmin/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="./assets/admin/paneladmin/css/scroller.bootstrap.min.css" rel="stylesheet">
  <!-- Custom styling plus plugins -->
  
  <link href="./assets/admin/paneladmin/css/icheck/flat/green.css" rel="stylesheet">
  <link href="./assets/admin/paneladmin/css/bootstrap.min.css" rel="stylesheet">
  
  <link href="./assets/admin/paneladmin/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="./assets/admin/paneladmin/js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="./assets/admin/paneladmin/js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="./assets/admin/paneladmin/js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="./assets/admin/paneladmin/js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="icon" href="./assets/images/pharm.png" type="image/png" sizes="16x16">

  <script src="./assets/admin/paneladmin/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="./assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

          <?php 
        } 
#####################################################################################################################

        function JsDataTabel(){
          ?>
          <script src="./assets/admin/paneladmin/js/bootstrap.min.js"></script>

          <!-- bootstrap progress js -->
          <script src="./assets/admin/paneladmin/js/progressbar/bootstrap-progressbar.min.js"></script>
          <!-- icheck -->
          <script src="./assets/admin/paneladmin/js/icheck/icheck.min.js"></script>

          <script src="./assets/admin/paneladmin/js/custom.js"></script>

          <!-- Datatables -->
        <!-- <script src="./assets/admin/paneladmin/js/datatables/js/jquery.dataTables.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/tools/js/dataTables.tableTools.js"></script> -->

        <!-- Datatables-->

        <script src="./assets/admin/paneladmin/js/datatables/jquery.dataTables.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/dataTables.bootstrap.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/dataTables.buttons.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/buttons.bootstrap.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/jszip.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/pdfmake.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/vfs_fonts.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/buttons.html5.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/buttons.print.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/dataTables.keyTable.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/dataTables.responsive.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/responsive.bootstrap.min.js"></script>
        <script src="./assets/admin/paneladmin/js/datatables/dataTables.scroller.min.js"></script>


        <!-- pace -->
        <script src="./assets/admin/paneladmin/js/pace/pace.min.js"></script>
        <script>
          var handleDataTableButtons = function() {
            "use strict";
            0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [{
                extend: "copy",
                className: "btn-sm"
              }, {
                extend: "csv",
                className: "btn-sm"
              }, {
                extend: "excel",
                className: "btn-sm"
              }, {
                extend: "pdf",
                className: "btn-sm"
              }, {
                extend: "print",
                className: "btn-sm"
              }],
              responsive: !0
            })
          },
          TableManageButtons = function() {
            "use strict";
            return {
              init: function() {
                handleDataTableButtons()
              }
            }
          }();
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable({
              ajax: "./assets/admin/paneladmin/js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>
      </body>

      </html>

      <?php 



    }

    function datepicker(){ ?>
    <script src="assets/datepicker/js/bootstrap-datepicker.min.js"></script>
    <link href="assets/datepicker/css/datepicker3.min.css" rel="stylesheet">
    <link href="assets/datepicker/css/datepicker.min.css" rel="stylesheet">

    <?php }


    ?> 