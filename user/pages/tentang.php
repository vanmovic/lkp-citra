<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Tentang LKP Citra Pratama</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="generator" content="motive"> 
	<!-- custom css -->
	<link rel="stylesheet" href="assets/user/css/style.css" />
	<!-- responsive -->
	<link rel="stylesheet" href="assets/user/css/responsive.css" />		
</head>
<body>  
	<!-- start header --> 
	<div id="header_social">
		<div class="container">
			<div class="row">
				<!-- company information -->
        <div class="col-lg-7 col-md-8 col-sm-9 col-xs-12">
          <div class="company_cell">
            <ul> 
              <li><i class="fa fa-envelope-o"></i> <a href="" >rynt_riyanto@yahoo.com </a></li>
              <li><i class="fa fa-phone"></i><a href="">(0272) 881018</a></li>
              <li><i class="fa fa-clock-o"></i><a href="" >Mon - Fri : 9:00 -1700</a></li> 
            </ul>
          </div>
        </div>				
				<!-- social menu -->
				<div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
					<div class="cotact_social top-social pull-right">
						<div class="footer_social pull-right">
							<a href="http://www.facebook.com" class="i fa fa-facebook"></a>
							<a href="http://www.twitter.com" class="i fa fa-twitter"></a>
							<a href="http://www.google.com" class="i fa fa-google-plus"></a>
							<a href="http://www.pinterest.com" class="i fa fa-pinterest"></a>
							<a href="http://www.rss.com" class="i fa fa-rss"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- header stricky menu -->
	<header class="header stricky"> 
		<div class="service_page">
			<div class="container">
				<section id="manu_area" class="manu_service">
				<h2 class="hide">menu</h2>
				<div class="navbar" role="navigation"> 
				  <div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					  <span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>
					<!-- logo here -->
                 </br><a href="home"><img width="250px" height="70px" src="assets/images/banner.png" alt="Sistem Informasi Akademik Citra Pratama" title="Sistem Informasi Akademik Citra Pratama"></a>
          </div>
				  
				  <!-- Main menu aea -->
				  <div class="navbar-collapse collapse">  
					<!-- Right nav -->
				 <ul class="nav navbar-nav main-menu scroll-menu">  
            <li class=""><a href="/lkp-citra/home">HOME<span class="border"></span><span class="caret caret-home"></span></a> 
            </li>
            <li><a href="tentang">TENTANG<span class="border"></span></a>
              <ul class="dropdown-menu hidesubmenu">  </ul>
            </li>
             <li><a href="mentor">MENTOR<span class="border"></span><span class="caret caret-home"></span></a>
            </li>
            <li><a href="contact">HUBUNGI KAMI<span class="border"></span></a>
            </li>
					</ul> 
				  </div> 
				</div>  
			</section>  
			</div>
		</div>  
    </header> 
	<!-- start header --> 
	<!-- page title area -->


	<!-- end area -->
	<section id="great_area" class="section_speach">
		<div class="container">
			<div class="row">
				<div class="great about_area">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="about_single">
							<div class="img-holder">
								<img src="assets/user/img/page/about/about_bg.jpg" alt="" />
								<div class="overlay">
									<a href="#"><i class="fa fa-link"></i></a>
								</div>
							</div>
						</div>   
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="great_right">
							<h2>Tentang</h2>
							<h3>LKP Citra Pratama</h3>
	<p>LKP Citra Pratama adalah lembaga bimbingan belajar dan pelatihan di Kecamatan Weru Sukoharjo. Berdiri sejak 31 Mei 2002, kami bergerak di bidang pendidikan.</p> 
<p>LKP Citra Pratama menghadirkan kursus dan pelatihan di antaranya: Bimbingan belajar SD/SMP/SMU, kursus operator komputer, dan kursus setir mobil.</p>
							 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- -->
	<!-- Project counter area start -->
	<section id="counter_area"> 
		<h2 class="hide">title</h2>
		<div class="Counter counter-one">
			<ul>
				<li>
					<div class="icon_text">
						<span><b>Program: </b><br></span>
					</div>
				</li>
			</ul>
		</div>
		<div class="Counter counter-two">
			<ul>
				<li>
					<div class="icon_text"></br>
						<div class="title_counter">Bimbel </br> SD / SMP / SMU</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="Counter counter-one">
			<ul>
				<li>
					<div class="icon_text"></br>
						<div class="title_counter">Kursus </br> Operator Komputer</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="Counter counter-two">
			<ul>
				<li>
					<div class="icon_text"></br>
						<div class="title_counter">Kursus </br> Setir Mobil</div>
					</div>
				</li>
			</ul>
		</div>  
	</section>
	<!-- Project counter area end --> 
	<!-- team area start -->
	<div id="team_area">
		<div class="container">
			<div class="row">
				<div class="team_title about_span text-center">
					<h3>VISI dan MISI</h3>
					<span></span>
					<h2>Visi</h2>
					<p>Terwujutnya lembaga pendidikan unggulan yang berbasis pada iman Taqwa dan Ilmu Pengetahuan-Teknologi dan menjadi wahana layanan pendidikan bagi masyarakat untuk meningkatkan kualitas sumber daya manusia. </p>

					<h2>Misi</h2>
					<p>1) Menyiapkan SDM berkualitas.</p>
					<p>2) Meningkatkan kwalitas pengelola kelembagaan.</p>
					<p>3) Meningkatkan kwalitas tengaga pendidik dan tenaga kependidikan.</p>
					<p>4) Menerapkan pembelajaran actual.</p>
					<p>5) Memenuhi sarana prasarana yang menujang proses pembelajaran.</p>

					<h3>Alamat</h3>
					<span></span>
					<p><a href="" class="i fa fa-map-marker"></a> JL. Kapten Patimura, No. 16, Ngreco, Weru, 57562, Tawang, Sukoharjo, Kabupaten Sukoharjo, Jawa Tengah 57561<br />

     <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.341078390133!2d110.75792431432355!3d-7.753601994411697!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a39b52daa0819%3A0xa3f3ff964d5250e4!2sLKP+Citra+Pratama!5e0!3m2!1sen!2sid!4v1496940181572" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></p>

				</div>
				
			
			</div>
		</div>
	</div>
	<!-- team area start -->
	
	 <!---footer start area -->
  <section id="footer_area">
    <div class="container">
      <div class="row">
        <div class="footer_content">
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer_spacing">
            <div class="single_footer">
 <a href="login"><img src="assets/images/bawah.png" style="padding-top:20px;" alt="logo_white_big" class="alignnone size-full wp-image-12908"></a>
            </div>
                      
            
          </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer_spacing">
            <div class="column-three">
            <h2>LKP Citra Pratama</h2>
<p>LKP Citra Pratama adalah lembaga bimbingan belajar dan pelatihan di Kecamatan Weru Sukoharjo. Berdiri sejak 31 Mei 2002, kami bergerak di bidang pendidikan.</p> 
<p>LKP Citra Pratama menghadirkan kursus dan pelatihan di antaranya: Bimbingan belajar SD/SMP/SMU, kursus operator komputer, dan kursus setir mobil.</p>
            </div>

                     <div class="footer_social">
              <a href="http://www.facebook.com" class="i fa fa-facebook"></a>
              <a href="http://www.twitter.com" class="i fa fa-twitter"></a>
              <a href="http://www.google.com" class="i fa fa-google-plus"></a>
              <a href="http://www.pinterest.com" class="i fa fa-pinterest"></a>
              <a href="http://www.rss.com" class="i fa fa-rss"></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer_spacing">
            <div class="column-three">
              <h2>Alamat</h2> 
              <a href="" class="i fa fa-map-marker"></a> JL. Kapten Patimura, No. 16, Ngreco, Weru, 57562, Tawang, Sukoharjo, Kabupaten Sukoharjo, Jawa Tengah 57561<br />
              <a href="" class="i fa fa-envelope-o"></a> rynt_riyanto@yahoo.com <br />
            </div>

          </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer_spacing">
            <div class="column-three">

              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3953.226231685196!2d110.748676!3d-7.765817000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa3f3ff964d5250e4!2sLKP+Citra+Pratama!5e0!3m2!1sen!2sid!4v1497277741590" width="250" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

          </div>

      </div>
    </div>
    <hr class="horizenal" /> 
    <div class="container">
      <div class="row">
        <div class="copy_right_area">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="copy_left">
              <p>&copy;Copyright 2017 <span>LKP Citra Pratama</span></p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
            <div class="copy_right">
              <ul>
                <li><a href="">Home</a></li>
                <li><a href="">FAQ</a></li>
                <li><a href="">About</a></li>
                <li><a href="">Contact</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- footer end area -->
	<!-- footer end area -->
	<!-- jquery library -->
	<script src=" js/jquery.min.js"></script>  
 	<!-- bootstrap -->
	<script src=" js/bootstrap.min.js"></script> 
	<!-- MixIt UP JS -->
	<script src=" js/jquery.mixitup.min.js"></script> 
	<script src=" js/masonry.pkgd.min.js"></script> 
	<!-- jQuery sticky -->
    <script src=" js/jquery.sticky.js"></script> 
	<!-- Count To JS -->
	<script src=" js/jquery.countTo.js"></script> 
	<script src=" js/jquery.appear.js"></script> 
	<!-- iSotope JS --> 
	<script src=" js/isotope.pkgd.min.js"></script> 
	<!-- owlcarousel -->
	<script src=" js/owl.carousel.js"></script> 
	  <!-- Gmap Helper -->
	<script src="http://maps.google.com/maps/api/js"></script>
	<script src=" js/gmap.js"></script> 
	<!-- SmartMenus jQuery plugin -->
    <script src=" js/jquery.smartmenus.js"></script> 
    <script src=" js/jquery.smartmenus.bootstrap.js"></script> 
	<!-- my js -->
	<script src=" js/main.js"></script>		
</body>
</html> 