<!-- user login -->
<?php
require ( __DIR__ . '/init.php');
checkUserAuth();
checkUserRole(array(10,1,0));

// TEMPLATE CONTROL
$ui_register_page     = 'siswa';
$ui_register_assets   = array('datepicker');

// LOAD HEADER
loadAssetsHead('Update Data Siswa');

//LOAD DATA

# SIMPAN DATA PADA FORM, Jika saat Sumbit ada yang kosong (lupa belum diisi)
$edit = mysql_query("SELECT * FROM siswa WHERE  id_siswa='$_GET[id]'");
$rowks  = mysql_fetch_array($edit);

?>

<body>

  <?php
  // LOAD MAIN MENU
  loadMainMenu();
  ?>

  <!-- page content -->
  <div class="uk-container uk-container-center uk-margin-large-top">
    <div class="uk-grid" data-uk-grid-margin data-uk-grid-match>
      <div class="uk-width-medium-1-6 uk-hidden-small">
        <?php loadSidebar() ?>
      </div>
      <div class="uk-width-medium-5-6 tm-article-side">
        <article class="uk-article">
          <div class="uk-vertical-align uk-text-right uk-height-1-1">
            <img class="uk-margin-bottom" width="500px" height="50px" src="assets/images/banner.png" alt="Sistem Informasi Akademik SD N II Manangga" title="Sistem Informasi Akademik SD N II Manangga">
          </div>
          <hr class="uk-article-divider">
          <h1 class="uk-article-title">Manajemen Siswa <span class="uk-text-large">{ Lihat Profil Siswa }</span></h1>
          <br>
          <a href="./guru.siswa" class="uk-button uk-button-primary uk-margin-bottom" type="button" title="Kembali ke Manajemen Siswa"><i class="uk-icon-angle-left"></i> Kembali</a>
          <!-- <hr class="uk-article-divider"> -->
          <form id="formsiswa" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                <form id="formsiswa" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
                  <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">


                      <div class="uk-grid">
                        <div class="uk-width-3-10">
                         <div class="item form-group">


                          <div class="item form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div class="col-lg-8">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="gallery/siswa/<?=$rowks['foto'];?>"></div>
                                  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                  <div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="uk-panel uk-panel-box"> 
                        <table class="uk-table uk-table-hover  uk-table-condensed"><tr><td><code class="title">Data Pribadi Siswa</code></td><td></td><td></td><td></td><td width="70"><?php if (isset($_SESSION['administrator'])) { ?><li><a href="siswa.update?id=<?php echo $rowks[id_siswa]?>" ><i  class="uk-icon-pencil"></i> Edit</a></li><?php }?></td> </tr></table>  



                        <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nis">NIS<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="nis" name="nis" value="<?php echo $rowks['nis'];?>" required="required" class="form-control col-md-7 col-xs-12">

                         </div>
                       </div>



                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nm_siswa">Nama Siswa<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="nm_siswa" name="nm_siswa" value="<?php echo $rowks['nm_siswa'];?>" required="required" class="form-control col-md-7 col-xs-12">

                         </div>
                       </div>

                       <?php if (isset($_SESSION['administrator'])) { ?>
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="password" name="password" value="<?php echo $rowks['password'];?>" required="required" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>


                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password1">Konfirmasi Password<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="password1" name="password1" value="<?php echo $rowks['password'];?>" required="required" class="form-control col-md-7 col-xs-12">

                         </div>
                       </div>

                       <?php }?>

                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tempat_lahir">Tempat Lahir<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="tempat_lahir" name="tempat_lahir" value="<?php echo $rowks['tempat_lahir'];?>" required="required" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>



                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_tgl_lahir">Tanggal Lahir<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="date_tgl_lahir" name="date_tgl_lahir" value="<?php echo  date('d/m/Y', strtotime($rowks['date_tgl_lahir'] )); ?>" required="required" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>



                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Jenis Kelamin<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="password" name="password" value="<?php echo $rowks['jns_kelamin'];?>" required="required" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>

                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat Rumah<span class="required">*</span>
                         </label>

                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="alamat" name="alamat" value="<?php echo $rowks['alamat'];?>" required="required" class="form-control col-md-7 col-xs-12">

                         </div>
                       </div>      
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="email" name="email" value="<?php echo $rowks['email'];?>" required="required" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>       
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_hp">No. HP<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly="readonly"  type="text" id="no_hp" name="no_hp" value="<?php echo $rowks['no_hp'];?>" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>      
                     </div>


                   </tr>
                 </tbody>
               </table>

             </div></div>

           </div>
         </div>
       </div>
     </div>
   </div>
 </div>

 <script src="assets/validator/js/bootstrapValidator.min.js" type="text/javascript"></script>
 <link rel="stylesheet" href="/vendor/formvalidation/css/formValidation.min.css">
 <link rel="stylesheet" href="/asset/css/demo.css">
 <script src="/vendor/formvalidation/js/formValidation.min.js"></script>
 <script src="/vendor/formvalidation/js/framework/uikit.min.js"></script>

 <script src="assets/validator/js/bootstrapValidator.min.js" type="text/javascript"></script>
 <link rel="stylesheet" href="/vendor/formvalidation/css/formValidation.min.css">
 <link rel="stylesheet" href="/asset/css/demo.css">
 <script src="/vendor/formvalidation/js/formValidation.min.js"></script>
 <script src="/vendor/formvalidation/js/framework/uikit.min.js"></script>

 <script type="text/javascript">
   var formsiswa = $("#formsiswa").serialize();
   var validator = $("#formsiswa").bootstrapValidator({
    framework: 'bootstrap',
    feedbackIcons: {
      valid: "glyphicon glyphicon-ok",
      invalid: "glyphicon glyphicon-remove", 
      validating: "glyphicon glyphicon-refresh"
    }, 
    excluded: [':disabled'],
    fields : {

      nis : {
       validators: {
        notEmpty: {
         message: 'Harus Isi NIS'
       },
       stringLength: {
        min: 1,
        max: 5,
        message: 'NIP maksimal 5 angka.'
      },

    }
  }, 
  nm_siswa: {
    message: 'Nama Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Nama Harus Diisi'
      },
      stringLength: {
        min: 1,
        max: 50,
        message: 'Nama Harus Lebih dari 1 Huruf dan Maksimal 50 Huruf'
      },
      regexp: {
        regexp: /^[a-zA-Z ]+$/,
        message: 'Karakter Yang Boleh Digunakan hanya huruf'
      },
    }
  },
  password: {
    message: 'Data Password Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Password Harus Diisi'
      },
      stringLength: {
        min: 1,
        max: 30,
        message: 'Nama kelurahan Harus Lebih dari 1 Huruf dan Maksimal 30 Huruf'
      },
      different: {
        field: 'email',
        message:'Password Harus Beda dengan Email'
      },          
    }
  },
  password1: {
    message: 'Data Password Tidak Benar',
    validators: {
      identical:{
        field:'password',
        message: 'Konfirmasi Password Harus Sama Dengan Password'
      },
      notEmpty: {
        message: 'Password Harus Diisi'
      },
      stringLength: {
        min: 1,
        max: 30,
        message: 'Nama kelurahan Harus Lebih dari 1 Huruf dan Maksimal 30 Huruf'
      },
      different: {
        field: 'email',
        message:'Password Harus Beda dengan Email'
      },
    }
  },
  tmpt_lahir : {
    validators: {
      notEmpty: {
        message: 'Harus diisi tempat lahir'
      }
    }
  },    
  jns_kelamin : {
    validators: {
      notEmpty: {
        message: 'Harus Pilih Jenis Kelamin'
      }
    }
  }, 
  agama : {
    validators: {
      notEmpty: {
        message: 'Harus Pilih Agama'
      }
    }
  },    
  prov : {
    validators: {
      notEmpty: {
        message: 'Harus Pilih Provinsi'
      }
    }
  },    
  kota : {
    validators: {
      notEmpty: {
        message: 'Harus Pilih Kabupaten'
      }
    }
  }, 
  id_kec : {
    validators: {
      notEmpty: {
        message: 'Harus Pilih Kecamatan'
      }
    }
  }, 
  id_kel : {
    validators: {
      notEmpty: {
        message: 'Harus Pilih Kelurahan'
      }
    }
  }, 
  alamat : {
    message: 'Alamat Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Alamat Harus Diisi'
      },
      stringLength: {
        min: 10,
        max: 100,
        message: 'Alamat Harus Lebih dari 10 Huruf dan Maksimal 100 Huruf'
      },
    }
  }, 
  no_hp: {
    message: 'No HP Tidak Benar',
    validators: {
      notEmpty: {
        message: 'No HP Harus Diisi'
      },
      stringLength: {
        min: 10,
        max: 30,
        message: 'No Hp Harus Lebih dari 1 Huruf dan Maksimal 30 Huruf'
      },
      regexp: {
        regexp: /^[0-9+]+$/,
        message: 'Format Tidak Benar'
      },

    }
  },
  email: {
    validators:{
      notEmpty: {
        message: 'Email Harus Diisi'
      },
      emailAddress:{
        message: 'Email Tidal valid'
      },

    }
  },

  tahun_masuk: {
    message: 'Tahun Masuk Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Tahun Masuk Harus Diisi'
      },
      stringLength: {
        min: 4,
        max: 4,
        message: 'Tahun Masuk Harus 4 Digit'
      },
      regexp: {
        regexp: /^[0-9+]+$/,
        message: 'Format Tidak Benar'
      },

    }
  },
  tahun_keluar: {
    message: 'Tahun Keluar Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Tahun Keluar Harus Diisi'
      },
      stringLength: {
        min: 4,
        max: 4,
        message: 'Tahun Keluar Harus 4 Digit'
      },
      regexp: {
        regexp: /^[0-9+]+$/,
        message: 'Format Tidak Benar'
      },

    }
  },


  nm_bapak: {
    message: 'Nama Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Nama Harus Diisi'
      },
      stringLength: {
        min: 1,
        max: 50,
        message: 'Nama Harus Lebih dari 1 Huruf dan Maksimal 50 Huruf'
      },
      regexp: {
        regexp: /^[a-zA-Z ]+$/,
        message: 'Karakter Yang Boleh Digunakan hanya huruf'
      },
    }
  },
  pekerjaan_bapak: {
    message: 'Pekerjaan Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Pekerjaan Harus Diisi'
      },
      stringLength: {
        min: 1,
        max: 50,
        message: 'Pekerjaan Harus Lebih dari 1 Huruf dan Maksimal 50 Huruf'
      },
      regexp: {
        regexp: /^[a-zA-Z ]+$/,
        message: 'Karakter Yang Boleh Digunakan hanya huruf'
      },
    }
  },
  gaji_bapak: {
    message: 'Gaji Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Gaji Harus Diisi'
      },
      stringLength: {
        min: 0,
        max: 30,
        message: 'Gaji Harus Lebih dari 1 Huruf dan Maksimal 30 Huruf'
      },
      regexp: {
        regexp: /^[0-9+]+$/,
        message: 'Format Tidak Benar'
      },
    }
  },
  nohp_bapak: {
    message: 'No HP Tidak Benar',
    validators: {
      notEmpty: {
        message: 'No HP Harus Diisi'
      },
      stringLength: {
        min: 10,
        max: 30,
        message: 'No Hp Harus Lebih dari 1 Huruf dan Maksimal 30 Huruf'
      },
      regexp: {
        regexp: /^[0-9+]+$/,
        message: 'Format Tidak Benar'
      },
    }
  },
  nm_ibu: {
    message: 'Nama Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Nama Harus Diisi'
      },
      stringLength: {
        min: 1,
        max: 50,
        message: 'Nama Harus Lebih dari 1 Huruf dan Maksimal 50 Huruf'
      },
      regexp: {
        regexp: /^[a-zA-Z ]+$/,
        message: 'Karakter Yang Boleh Digunakan hanya huruf'
      },
    }
  },
  pekerjaan_ibu: {
    message: 'Pekerjaan Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Pekerjaan Harus Diisi'
      },
      stringLength: {
        min: 1,
        max: 50,
        message: 'Pekerjaan Harus Lebih dari 1 Huruf dan Maksimal 50 Huruf'
      },
      regexp: {
        regexp: /^[a-zA-Z ]+$/,
        message: 'Karakter Yang Boleh Digunakan hanya huruf'
      },
    }
  },
  gaji_ibu: {
    message: 'Gaji Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Gaji Harus Diisi'
      },
      stringLength: {
        min: 0,
        max: 30,
        message: 'Gaji Harus Lebih dari 1 Huruf dan Maksimal 30 Huruf'
      },
      regexp: {
        regexp: /^[0-9+]+$/,
        message: 'Format Tidak Benar'
      },
    }
  },
  nohp_ibu: {
    message: 'No HP Tidak Benar',
    validators: {
      notEmpty: {
        message: 'No HP Harus Diisi'
      },
      stringLength: {
        min: 10,
        max: 30,
        message: 'No Hp Harus Lebih dari 1 Huruf dan Maksimal 30 Huruf'
      },
      regexp: {
        regexp: /^[0-9+]+$/,
        message: 'Format Tidak Benar'
      },
    }
  },
  alamat : {
    message: 'Alamat Tidak Benar',
    validators: {
      notEmpty: {
        message: 'Alamat Harus Diisi'
      },
      stringLength: {
        min: 10,
        max: 100,
        message: 'Alamat Harus Lebih dari 10 Huruf dan Maksimal 100 Huruf'
      },
    }
  }, 

}
});
</script>

</body>

<?php
// LOAD FOOTER
loadAssetsFoot();

ob_end_flush();
?>
