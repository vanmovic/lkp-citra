<?php
// user login
require ( __DIR__ . '/init.php');
checkUserAuth();
checkUserRole(array(10));

/*template control*/
$ui_register_page     = 'siswa';
$ui_register_assets   = array('datepicker');

/*load header*/
loadAssetsHead('Tambah Master Data Siswa');

/*form processing*/
if (isset ($_POST["siswa_simpan"])) { 

    // baca variabel

  $nis     = $_POST['nis'];
  $password     = $_POST['password'];
  $password1  = $_POST['password1'];
  $nm_siswa     = $_POST['nm_siswa'];
  $tempat_lahir     = $_POST['tempat_lahir'];
  $date_tgl_lahir0  = $_POST['date_tgl_lahir'];
  $date_tgl_lahir=ubahformatTgl($date_tgl_lahir0);
  $jns_kelamin     = $_POST['jns_kelamin'];
  $email = $_POST['email'];
  $no_hp  = $_POST['no_hp'];
  $telp     = $_POST['telp'];
  $id_user =3;
  $alamat              = $_POST['alamat'];



    // validation form kosong
  
  function compress_image($source_url, $destination_url, $quality) 
  { 
    $info = getimagesize($source_url); 
    if ($info['mime'] == 'image/jpeg') 
      $image = imagecreatefromjpeg($source_url); 
    elseif ($info['mime'] == 'image/gif') 
      $image = imagecreatefromgif($source_url); 
    elseif ($info['mime'] == 'image/png') 
      $image = imagecreatefrompng($source_url); 
    imagejpeg($image, $destination_url, $quality); 
    return $destination_url; 
  } 

  $nama_foto = $_FILES["file"]["name"];
      $file_sik_dipilih = substr($nama_foto, 0, strripos($nama_foto, '.')); // strip extention
      $bagian_extensine = substr($nama_foto, strripos($nama_foto, '.')); // strip name
      $ukurane = $_FILES["file"]["size"];

      $pesanError= array();
      if (trim($nis)=="") {
        $pesanError[]="Data <b>NIS</b> Masih Kosong.";
      }
      if (trim($password)=="") {
        $pesanError[]="Data <b>Password</b> Masih Kosong.";
      }
      if (trim($password1)=="") {
        $pesanError[]="Data Konfirmasi<b>Password</b> masih kosong.";
      }
      if (trim($nm_siswa)=="") {
        $pesanError[]="Data <b>Nama Siswa</b> Masih Kosong.";
      }
      if (trim($tempat_lahir)=="") {
        $pesanError[]="Data <b>Tempat Lahir</b> Masih Kosong.";
      }
      if (trim($date_tgl_lahir)=="") {
        $pesanError[]="Data <b>Tanggal Lahir</b> Masih Kosong.";
      }
      if (trim($jns_kelamin)=="") {
        $pesanError[]="Data <b>Jenis Kelamin</b> Masih Kosong.";
      }
      if (trim($email)=="") {
        $pesanError[]="Data <b>Email</b> Masih Kosong.";
      }
      if (trim($no_hp)=="") {
        $pesanError[]="Data <b>Nomor Telepon</b> Masih Kosong.";
      }
      if (trim($id_user)=="") {
        $pesanError[] = "Data <b>id_user</b> tidak boleh kosong !";    
      }
      if (empty($file_sik_dipilih)){
        $pesanError[] = "Anda Belum Memilih Foto !";    
      }

    // validasi kode kelas pada database
      $cekSql ="SELECT * FROM siswa WHERE nis='$nis'";
      $cekQry = mysql_query($cekSql) or die("Error Query:".mysql_error());
      if (mysql_num_rows($cekQry)>=1) {
        $pesanError[]= "Maaf, siswa dengan NIS <b>$nis</b> Sudah Ada, ganti dengan nama lain";
      }

    // jika ada error dari validasi form
      if (count($pesanError)>=1) {
        echo "
        <div class='alert alert-danger alert-dismissable'>
          <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
          $noPesan= 0;
          foreach ($pesanError as $indeks => $pesan_tampil) {
            $noPesan++;
            echo "&nbsp;&nbsp; $noPesan. $pesan_tampil<br>";
          }
          echo "</div><br />";
        }
        else{

          if(($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")){
            $lokasi = 'gallery/siswa/';

            $file = md5(rand(1000,1000000000))."-".$nama_foto;
            $newfilename = $file . $bagian_extensine;
            $jeneng=str_replace(' ','-',$file);
            $url = $lokasi . $jeneng;
            $filename = compress_image($_FILES["file"]["tmp_name"], $url, 80); 

            $query = mysql_query("INSERT INTO siswa 
              SET id_user ='$id_user', 
              nis='$nis', 
              password='$password',
              nm_siswa='$nm_siswa',
              tempat_lahir='$tempat_lahir',
              date_tgl_lahir='$date_tgl_lahir',
              jns_kelamin='$jns_kelamin',
              alamat='$alamat',
              email='$email',
              no_hp='$no_hp',
              foto='$jeneng'
              
              ") or die(mysql_error());

          }
          if ($query){
            header('location: ./siswa');
          }
          else { $error = "Uploaded image should be jpg or gif or png"; } 

        }
      }



    // simpan pada form, dan jika form belum terisi
      $datanis  = isset($_POST['nis']) ? $_POST['nis'] : '';
      $datapassword  = isset($_POST['password']) ? $_POST['password'] : '';
      $datanamasiswa  = isset($_POST['nm_siswa']) ? $_POST['nm_siswa'] : '';
      $datatempatlahir  = isset($_POST['tempat_lahir']) ? $_POST['tempat_lahir'] : '';
      $datatanggallahir  = isset($_POST['date_tgl_lahir']) ? $_POST['date_tgl_lahir'] : '';
      $datajeniskelamin  = isset($_POST['jns_kelamin']) ? $_POST['jns_kelamin'] : '';
      $dataalamat  = isset($_POST['alamat']) ? $_POST['alamat'] : '';
      $dataemail  = isset($_POST['email']) ? $_POST['email'] : '';
      $datanohp  = isset($_POST['no_hp']) ? $_POST['no_hp'] : '';
      $dataalamat  = isset($_POST['alamat']) ? $_POST['alamat'] : '';
      ?>
      
      <script type="text/javascript">
        function convertAngkaNIS(objek) {

          a = objek.value;
          b = a.replace(/[^\d]/g,"");

          objek.value = b;

        }            
      </script>
      <script type="text/javascript">
        function convertAngkaHP(objek) {

          a = objek.value;
          b = a.replace(/[^\d]/g,"");

          objek.value = b;

        }            
      </script>
      <script type="text/javascript">
        function convertAngkaGaji(objek) {

          a = objek.value;
          b = a.replace(/[^\d]/g,"");

          objek.value = b;

        }            
      </script>
      <script type="text/javascript">
        function convertAngka(objek) {

          a = objek.value;
          b = a.replace(/[^\d]/g,"");

          objek.value = b;

        }            
      </script>
      <body>

        <?php
  // LOAD MAIN MENU
        loadMainMenu();
        ?>

        <div class="uk-container uk-container-center uk-margin-large-top">
          <div class="uk-grid" data-uk-grid-margin data-uk-grid-match>
            <div class="uk-width-medium-1-6 uk-hidden-small">
              <?php loadSidebar() ?>
            </div>
            <div class="uk-width-medium-5-6 tm-article-side">
              <article class="uk-article">
                <div class="uk-vertical-align uk-text-right uk-height-1-1">
                  <img class="uk-margin-bottom" width="500px" height="50px" src="assets/images/banner.png" alt="Sistem Informasi Akademik SD N II Manangga" title="Sistem Informasi Akademik SD N II Manangga">
                </div>
                <hr class="uk-article-divider">
                <h1 class="uk-article-title">Siswa <span class="uk-text-large">{ Tambah Master Data Siswa }</span></h1>
                <br>
                <a href="./siswa" class="uk-button uk-button-primary uk-margin-bottom" type="button" title="Kembali ke Manajemen Siswa"><i class="uk-icon-angle-left"></i> Kembali</a>

                <hr class="uk-article-divider">

                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-1">
                   <form id="formsiswa" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">

                    <div class="uk-grid" data-uk-grid-margin>
                      <div class="uk-width-medium-1-1">


                        <div class="uk-grid">
                          <div class="uk-width-3-10">
                           <div class="item form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div class="col-lg-8">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="./assets/fileupload/images/nopict.jpg" alt="" /></div>
                                  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                  <div>
                                    <span class="btn btn-file btn-primary btn-xs"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" accept="image/*" name="file" id="file" placeholder="file" /></span>
                                    <a href="#" class="btn btn-danger btn-xs fileupload-exists" data-dismiss="fileupload">Remove</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>                            
                        </div>


                      <div class="uk-panel uk-panel-box"> 
                       <code class="title">Data Pribadi Siswa</code>
                       <hr class="uk-article-divider">                
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nis">NIS<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nis" name="nis" value="<?php echo $datanis; ?>" onkeyup="convertAngkaNIS(this);" required="required" class="form-control col-md-7 col-xs-12">
                          <div class="reg-info">Contoh: 151601001. Wajib Diisi Minimal 9 Karakter (Digunakan sebagai username untuk login)</div>
                        </div>
                       </div>
                      <div class="item form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nm_siswa">Nama Siswa<span class="required">*</span>
                       </label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="nm_siswa" name="nm_siswa" value="<?php echo $datanamasiswa; ?>" required="required" class="form-control col-md-7 col-xs-12">
                        <div class="reg-info">Contoh: Ripa Gemah Nuripah</div>
                       </div>
                      </div>
                      <div class="item form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nip">Password<span class="required">*</span>
                       </label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="password" name="password" value="<?php echo $datapassword; ?>" required="required" class="form-control col-md-7 col-xs-12">
                        <div class="reg-info">Contoh: 55550.</div>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password1">Konfirmasi Password<span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="password1" name="password1" value="<?php echo $datapassword; ?>" required="required" class="form-control col-md-7 col-xs-12">
                        <div class="reg-info">Contoh: 55550.</div>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_tgl_lahir">Tanggal Lahir<span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="date_tgl_lahir" name="date_tgl_lahir" value="<?php echo $datatanggallahir; ?>" required="required" class="form-control col-md-7 col-xs-12" data-uk-datepicker="{format:'DD/MM/YYYY'}" >
                        <div class="reg-info">Format: <code>DD/MM/YYYY</code></div>
                        <div class="reg-info">Contoh: 31/12/1994</div>
                      </div>
                    </div>


                    <div class="item form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tempat_lahir">Tempat Lahir<span class="required">*</span>
                     </label>
                     <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" id="tempat_lahir" name="tempat_lahir" value="<?php echo $datatempatlahir; ?>" required="required" class="form-control col-md-7 col-xs-12">
                      <div class="reg-info">Contoh: Tasikmalaya.</div>
                    </div>
                  </div>


                  <div class="item form-group">
                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jns_kelamin">Jenis Kelamin<span class="required">*</span>
                   </label>
                   <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="jns_kelamin" id="jns_kelamin" value="<?php echo $datajeniskelamin; ?>" class="form-control col-md-7 col-xs-12">
                      <option value="">--- Pilih Jenis Kelamin --</option>
                      <option value="Laki-Laki">Laki-Laki</option>
                      <option value="Perempuan">Perempuan</option>
                    </select>
                  </div>
                </div>

                <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat Rumah<span class="required">*</span>
                 </label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="alamat" name="alamat" value="<?php echo $dataalamat; ?>" required="required" class="form-control col-md-7 col-xs-12">
                  <div class="reg-info">Contoh: Jalan Kenangan, Kota Barat, Depok, Yogyakarta.</div>
                </div>
              </div>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="email" name="email" value="<?php echo $dataemail; ?>" required="required" class="form-control col-md-7 col-xs-12">
                  <div class="reg-info">Contoh: ofa@gmail.com</div>
                </div>
              </div>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_hp">No. HP<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="no_hp" name="no_hp" value="<?php echo $datanohp; ?>" onkeyup="convertAngkaHP(this);" required="required" class="form-control col-md-7 col-xs-12">
                  <div class="reg-info">Contoh: 08123456789. Wajib Isi Data Nomor Handphone yang Dapat Dihubungi.</div>
                </div>
              </div>


              <div class="uk-form-row">
                <div class="uk-alert">Pastikan semua isian sudah terisi dengan benar!</div>
              </div>
              <div style="text-align:center" class="form-actions no-margin-bottom">
                <button type="submit" id="siswa_simpan" name="siswa_simpan" class="btn btn-success">Submit</button>

                </div>
              </form>    
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div>
</div>
<script src="assets/validator/js/bootstrapValidator.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/vendor/formvalidation/css/formValidation.min.css">
<link rel="stylesheet" href="/asset/css/demo.css">
<script src="/vendor/formvalidation/js/formValidation.min.js"></script>
<script src="/vendor/formvalidation/js/framework/uikit.min.js"></script>
<script type="text/javascript">
 var formsiswa = $("#formsiswa").serialize();
 var validator = $("#formsiswa").bootstrapValidator({
  framework: 'bootstrap',
  feedbackIcons: {
    valid: "glyphicon glyphicon-ok",
    invalid: "glyphicon glyphicon-remove", 
    validating: "glyphicon glyphicon-refresh"
  }, 
  excluded: [':disabled'],
  fields : {


    file : {
      validators : {
        notEmpty: {
          message: 'Belum Memilih Gambar'
        },
        file : {
          extention : 'jpeg,jpg,png',
          type : 'image/jpeg,image/png',
              //maxSize : 2097152, //2048*1024
              message : 'file tidak benar'
            }
          }
        } ,
        nis : {
         validators: {
          notEmpty: {
           message: 'Harus Isi NIS'
         },
         stringLength: {
          min: 9,
          max: 9,
          message: 'NIS 9 karakter.'
        },
        remote: {
          type: 'POST',
          url: 'remote/remote_siswa.php',
          message: 'Nama Siswa Telah Tersedia'
        },
      }
    }, 
    nm_siswa: {
      message: 'Nama Tidak Benar',
      validators: {
        notEmpty: {
          message: 'Nama Harus Diisi'
        },
        stringLength: {
          min: 1,
          max: 50,
          message: 'Nama Harus Lebih dari 1 Huruf dan Maksimal 50 Huruf'
        },
        regexp: {
          regexp: /^[a-zA-Z ]+$/,
          message: 'Karakter Yang Boleh Digunakan Hanya Huruf'
        },
      }
    },
    password: {
      message: 'Data Password Tidak Benar',
      validators: {
        notEmpty: {
          message: 'Password Harus Diisi'
        },


      }
    },
    password1: {
      message: 'Data Password Tidak Benar',
      validators: {
        identical:{
          field:'password',
          message: 'Konfirmasi Password Harus sama dengan Password'
        },
        notEmpty: {
          message: 'Password Harus Diisi'
        },


      }
    },
    tempat_lahir : {
      validators: {
        notEmpty: {
          message: 'Harus Diisi Tempat lahir'
        }
      }
    },    
    jns_kelamin : {
      validators: {
        notEmpty: {
          message: 'Harus Pilih Jenis Kelamin'
        }
      }
    }, 

    alamat : {
      message: 'Alamat Tidak Benar',
      validators: {
        notEmpty: {
          message: 'Alamat Harus Diisi'
        },
        stringLength: {
          min: 10,
          max: 100,
          message: 'Alamat Harus Lebih dari 10 Huruf dan Maksimal 100 Huruf'
        },
      }
    }, 
    no_hp: {
      message: 'No HP Tidak Benar',
      validators: {
        notEmpty: {
          message: 'No HP Harus Diisi'
        },
        stringLength: {
          min: 1,
          max: 30,
          message: 'No Hp Harus Lebih dari 1 Huruf dan Maksimal 30 Huruf'
        },
        regexp: {
          regexp: /^[0-9+]+$/,
          message: 'Format Tidak Benar'
        },
      }
    },
    email: {
      validators:{
        emailAddress:{
          message: 'Email Tidak Valid'
        },
        remote: {
          type: 'POST',
          url: './remote/remote_email_siswa.php',
          message: 'Email Sudah Tersedia'
        },
      }
    },
    


    

  }
});
</script>

</body>

<?php
// LOAD FOOTER
loadAssetsFoot();

ob_end_flush();
?>
