<!-- user login -->
<?php
require ( __DIR__ . '/init.php');
checkUserAuth();
checkUserRole(array(10,1,0,24));

// TEMPLATE CONTROL
$ui_register_page     = 'guru';
$ui_register_assets   = array('datepicker');

// LOAD HEADER
loadAssetsHead('Data Lihat Guru');
$edit = mysql_query("SELECT * FROM guru WHERE id_guru='$_GET[id]'");
$rowks  = mysql_fetch_array($edit);
?>
<body>

  <?php
  // LOAD MAIN MENU
  loadMainMenu();
  ?>

  <!-- page content -->
  <div class="uk-container uk-container-center uk-margin-large-top">
    <div class="uk-grid" data-uk-grid-margin data-uk-grid-match>
      <div class="uk-width-medium-1-6 uk-hidden-small">
        <?php loadSidebar() ?>
      </div>
      <div class="uk-width-medium-5-6 tm-article-side">
        <article class="uk-article">
          <div class="uk-vertical-align uk-text-right uk-height-1-1">
            <img class="uk-margin-bottom" width="500px" height="50px" src="assets/images/banner.png" alt="Sistem Informasi Akademik SD N II Manangga" title="Sistem Informasi Akademik SD N II Manangga">
          </div>
          <div class="uk-panel uk-panel-box">
           

          </div>
          <hr class="uk-article-divider">
          <h1 class="uk-article-title">Manajemen Data Guru <span class="uk-text-large">{ Tampil Profil Guru }</span></h1>
          <a href="./guru" class="uk-button uk-button-primary uk-margin-bottom" type="button" title="Kembali ke Manajemen Guru"><i class="uk-icon-angle-left"></i> Kembali</a> 
          <?php if (isset($_SESSION['administrator'])) { ?>

          <br>
          <br>
          <?php } ?>
          <!-- <hr class="uk-article-divider"> -->
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-1">
              <form id="formguru" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">

                <div class="uk-grid">
                  <div class="uk-width-3-10"><div class="uk-panel uk-panel-box"><div class="sia-profile">

                    <img src="gallery/guru/<?=$rowks['foto'];?>">
                    <br>
                    <br>
                    <p style="text-align:center" ;="" font-weight:bold;=""><b><?php echo $rowks['nm_guru'];?></b></p>
                    <p style="text-align:center" ;="" font-weight:bold;=""><b><?php echo $rowks['nip'];?></b></p>
                    <p style="text-align:center" ;="" font-weight:bold;=""></p>

                  </div></div></div>
                  <div class="uk-width-7-10">  <div class="uk-panel uk-panel-box"> <table class="uk-table uk-table-hover  uk-table-condensed"><tr><td></td><td></td><td></td><td></td><td width="70"><li>
                    <?php if (isset($_SESSION['administrator'])) { ?>
                    <a href="guru.update?id=<?php echo $rowks[id_guru]?>" ><i  class="uk-icon-pencil"></i> Edit</a><?php } ?>    </li></td> </tr></table> 

                    <table class="uk-table uk-table-hover  uk-table-condensed">
                      <tbody>
                        <tr>
                          <div class="item form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nip">NIP<span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                             <input readonly type="text" id="nip" name="nip" value="<?php echo $rowks['nip'];?>" required="required" class="form-control col-md-7 col-xs-12">
                             <div class="hidden">Contoh: 126500182411. Jumlah minimal 18 angka. Wajib diisi (Digunakan sebagai username untuk login sistem)</div>
                           </div> 
                         </div>
                         
                       </tr>
                       <tr>
                        <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nm_guru">Nama Guru<span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input readonly type="text" id="nm_guru" name="nm_guru" value="<?php echo $rowks['nm_guru'];?>" required="required" class="form-control col-md-7 col-xs-12">
                           <div class="hidden">Contoh: Fajar Nurrohmat. Jumlah minimal 1 huruf. Wajib diisi (Tuliskan nama saja, tidak dengan gelar akademik)</div>
                         </div>
                       </div>
                     </tr>
                     <tr>
                      <?php if (isset($_SESSION['administrator'])) { ?>
                      <div class="item form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password<span class="required">*</span>
                       </label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                         <input readonly type="text" id="password" name="password" value="<?php echo $rowks['password'];?>" required="required" class="form-control col-md-7 col-xs-12">
                       </div>
                     </div>
                   </tr>

                   <tr>
                     <div class="item form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password1">Konfirmasi Password<span class="required">*</span>
                       </label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                         <input readonly type="text" id="password1" name="password1" value="<?php echo $rowks['password'];?>" required="required" class="form-control col-md-7 col-xs-12">
                         <div class="hidden">Contoh: 126500182411. Jumlah minimal 6 karakter. Harus Sama dengan Password. Wajib diisi (Digunakan untuk login)</div>
                       </div>
                     </div>
                   </tr>
                   <?php } ?>
                   <tr>
                     <div class="item form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_tgl_lahir">Tanggal Lahir<span class="required">*</span>
                       </label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                         <input readonly required type="text" id="date_tgl_lahir" name="date_tgl_lahir" value="<?php echo  date('d/m/Y', strtotime($rowks['date_tgl_lahir'] )); ?>" class="form-control">
                       </div>
                     </div>
                   </tr>
                   <tr>
                    <div class="item form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tmpt_lahir">Tempat Lahir<span class="required">*</span>
                     </label>
                     <div class="col-md-6 col-sm-6 col-xs-12">
                       <input readonly type="text" id="tmpt_lahir" name="tmpt_lahir" value="<?php echo $rowks['tmpt_lahir'];?>" required="required" class="form-control col-md-7 col-xs-12">
                     </div>
                   </div>
                 </tr>
                 <tr>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jns_kelamin">Jenis Kelamin<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input readonly type="text" id="jns_kelamin" name="jns_kelamin" value="<?php echo $rowks['jns_kelamin'];?>"  class="form-control col-md-7 col-xs-12">
                      
                    </div>
                  </div>            
                </tr>
                <tr>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gelar_depan">Gelar Depan Non Akademik<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input readonly type="text" id="gelar_depan" name="gelar_depan" value="<?php echo $rowks['gelar_depan'];?>"  class="form-control col-md-7 col-xs-12">
                      
                    </div>
                  </div>            
                </tr>
                <tr>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gelar_depan_akademik">Gelar Depan Akademik<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input readonly type="text" id="gelar_depan_akademik" name="gelar_depan_akademik" value="<?php echo $rowks['gelar_depan_akademik'];?>"  class="form-control col-md-7 col-xs-12">
                      <div class="hidden">Kosongkan Jika Tidak Ada Gelar Depan Akademik</div>
                    </div>
                  </div>      
                </tr>
                <tr>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gelar_belakang">Gelar Belakang<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input readonly type="text" id="gelar_belakang" name="gelar_belakang" value="<?php echo $rowks['gelar_belakang'];?>"  class="form-control col-md-7 col-xs-12">
                      <div class="hidden">Kosongkan Jika Tidak Ada Gelar Belakang</div>
                    </div>
                  </div>      
                </tr>

                <tr>
                  <div class="item form-group">
                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="almt_sekarang">Alamat Rumah<span class="required">*</span>
                   </label>

                   <div class="col-md-6 col-sm-6 col-xs-12">
                     <input readonly type="text" id="almt_sekarang" name="almt_sekarang" value="<?php echo $rowks['almt_sekarang'];?>" required="required" class="form-control col-md-7 col-xs-12">
                     <div class="hidden">Wajib diisi data alamat rumah sekarang, isi data alamat rumah sekarang dengan lengkap</div>
                   </div>
                 </div>      
               </tr>
               <tr>
                <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_hp">No. HP<span class="required">*</span>
                 </label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                   <input readonly type="text" id="no_hp" name="no_hp" value="<?php echo $rowks['no_hp'];?>" required="required" class="form-control col-md-7 col-xs-12">
                   <div class="hidden">Wajib Isi Data No Hp</div>
                 </div>
               </div>      
             </tr>
             <tr>
              <div class="item form-group">
               <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
               </label>
               <div class="col-md-6 col-sm-6 col-xs-12">
                 <input readonly type="text" id="email" name="email" value="<?php echo $rowks['email'];?>" required="required" class="form-control col-md-7 col-xs-12">
                 <div class="hidden">Email Wajib Diisi </div>
               </div>
             </div>      
           </tr>
           <tr>
            
           </form>      
         </tr>
       </tbody>
     </table>

   </div></div>

 </div>
</div>
</div>
</div>
</div>
</div>


</body>

<?php
// LOAD FOOTER
loadAssetsFoot();

ob_end_flush();
?>
