<?php
session_start();
require ( __DIR__ . '/init.php');
checkUserAuth();
checkUserRole(array(0,1,2,10,24));

// TEMPLATE CONTROL
$ui_register_page = 'dashboard';

// LOAD HEADER
loadAssetsHead('Dashboard - Sistem Informasi Akademik LKP Citra Pratama');

// FORM PROCESSING
// ... code here ...
?>

<body>
	<?php
  // LOAD MAIN MENU
	loadMainMenu();
	?>
	<div class="uk-container uk-container-center uk-margin-large-top">

		<div class="uk-grid" data-uk-grid-margin data-uk-grid-match>

			<div class="uk-width-medium-1-6 uk-hidden-small">
				<?php loadSidebar() ?>
			</div>

			<div class="uk-width-medium-5-6 tm-article-side">
				<article class="uk-article">

					<div class="uk-vertical-align uk-text-right uk-height-1-1">
						<img class="uk-margin-bottom" width="500px" height="50px" src="assets/images/banner.png" alt="Sistem Informasi Akademik LKP Citra Pratama" title="Sistem Informasi Akademik LKP Citra Pratama">
					</div>

					<hr class="uk-article-divider">
					<h1 class="uk-article-title">Dashboard</h1>
					<br>


					<div class='uk-form-row'>
						<div class="alert alert-success" role="alert">
							Selamat datang di Sistem Informasi Akademik
						</div>
					</div>

					<div class="panel panel-info">
						<div class="panel-heading"><center></center></div>
						<div class="panel-body">
							
</div>
</div>
</div>
</div>
</div>
</body>

<?php
// ADDITIONAL SCRIPTS
$scripts = <<<'JS'
<script>
</script>

JS;

// LOAD FOOTER
loadAssetsFoot($scripts);

ob_end_flush();
?>